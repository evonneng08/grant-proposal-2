\documentclass[12pt]{scrartcl}
\usepackage[letterpaper]{geometry}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{flafter}
\usepackage{abstract}
\usepackage{float}
\usepackage{longtable}
\usepackage{multicol}
\usepackage{listings}
\usepackage{mathptmx}
\usepackage{caption}
\setlist[itemize]{itemsep=0mm}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage[capitalise]{cleveref}
\geometry{top=1.0in, bottom=1.0in, left=1.0in, right=1.0in}
\usepackage{setspace}
\usepackage{afterpage}
\usepackage[authoryear,round,longnamesfirst]{natbib}
\setkomafont{disposition}{\normalfont\bfseries}
\setlength\headsep{0.333in}
\bibliographystyle{oas}
\setcitestyle{aysep={},comma}
\newenvironment{DIFnomarkup}{}{}
\AtBeginDocument{%
    \crefname{table}{table}{tables}%
    \crefname{figure}{fig.}{fig.}
    \Crefname{figure}{Fig.}{Fig.}
}

%%%% TITLE %%%%
\title{DROWNING DETECTION THROUGH MACHINE LEARNING AND ITS APPLICATIONS}
\subtitle{Moore Cohort}
%%%% AUTHOR + DATE %%%%
\author{
    Elena Arnold \and
    Souvik Banerjee \and
    Sean Geiger \and
    Calvin Ly \and
    Evonne Ng \and
    Nolan Daniels
}
\date{\today}

\begin{document}
  \begin{DIFnomarkup}
    \section*{Rebuttals}
    
    \clearpage
  \end{DIFnomarkup}
    \maketitle
    \begin{abstract}
        As the second leading cause of death among children ages 14 and under \citep{Evonne-S1-31433556}, drowning is a universal concern. 
        Distracting surroundings cause lifeguards, who rely primarily on vision to detect drowning victims, to often overlook or miss important signals indicating distress. 
        This proposal thus aims to explore the potential of machine learning in developing a program to accurately identify drowning incidents through camera feeds. 
        In this study, a drowning incident is any potentially dangerous situation in which a should lifeguard intervene.   
        This study dips into both fields of human body recognition and artificial intelligence. 
        Over six months, most of the data will be collected by installing cameras at a variety of high risk public pools to capture both normal behavior and drowning incidents. 
        After classifying the data, the focus is to ``train'' a recurrent neural network to identify drowning at a high degree of accuracy and precision. 
        This will involve detailed abstraction and pattern recognition to generate appropriate evaluations of each individual's status in the pool. 
        The primary limitation in this study is the quantitative restrictions on the collection of data. In fact, given a six month period, it is probable that too few incidents of drowning occur, making it difficult to advance the algorithm. 
        If such is the case, the duration of this study may have to be extended. 
        Nevertheless, this study is holistically relevant to scientific research concerning both human behavior recognition and artificial intelligence. 
        Furthermore, if successful, the results lead logically towards an effective drowning detection and lifeguarding system.
    \end{abstract}
    
    \section{Introduction}
    
    This research project will expand the current applications of machine learning and computer vision. 
    Currently, it is possible to track people in a crowd with a single uncalibrated cameras \citep{Elena-S1-31433556}. 
    Our research will add on to this by trying to analyze body language as well as position. 
    The research will also look into distortion caused by the water and how it affects tracking algorithms. 
    Once the system is able to track people machine learning will be used to teach the artificial intelligence what drowning looks like. 
    The artificial intelligence will have to make decisions based on the constantly changing system. 
    Dynamic decision making is already being explored through robot soccer to allow the robots to judge what the next best move is \citep{Elena-S2-31433556}. 
    Using the data from our study, we will modify these dynamic decision making processes to allow the artificial intelligence to judge which behaviors most closely resemble drowning. 
    
    \section{Originality and significance}
    
    According to the World Health Organization, an estimated 372,000 people died from drowning in 2012, making it the third leading cause of unintentional injury death worldwide, and the second leading cause of death for children \citep{Nolan-S1-WHODrowningStats}.  
    While drowning is a threat to people of all regions and economies, children and males are at a higher risk of a fatal accident.  
    In addition to the burden placed on humanity by the loss of life, drowning has cost impacts.  
    ``Coastal drowning in the United States alone accounts for \$273 million [annually] in direct and indirect costs'' \citep{Nolan-S1-WHODrowningStats}.  
    The threat posed by drowning can be lessened by a number of actions, including installing barriers to prevent access to water hazards, teaching children to swim with an emphasis on safety, and staffing lifeguards at public pools and beaches.  
    However, these methods are far from perfect.  
    According to the Centers for Disease Control and Prevention, even in the United States, a highly developed country where virtually all methods of preventing drowning have already been implemented, drowning still accounts for ``3,533 annual non-boating related deaths'' \citep{Nolan-S2-CDCDrowningStats}.  
    This research will be the first in exploring ways to aid lifeguards using artificial intelligence.
    By implementing our system of cameras at pools and beaches across the world, we could take the next step in eliminating the burden posed by drowning.  
    In addition, the advances in our current understandings of artificial intelligence and machine learning that we expect to gain from our research will be beneficial to new technologies that deal these topics.
    
    \section{Objective}
    
    The objective of this research is to create a functioning system of cameras and and an artificial intelligence that can send alerts when someone exhibits signs of drowning within a pool. 
    This system should be able to track multiple people, monitor the behaviors of each person, and make judgments on the state of the person based on their body language. 
    In total, this study will take two years. The first six months, March to September in order to encompass summer, will be spent gathering video data on 100 pools. 
    During this time, research will be done on monitoring and identifying the body language of multiple people. 
    The other one and a half years will consist of data analysis and determining the heuristics needed to judge whether a person is drowning.
    Data collection will continue during this time period.
    
    \section{Methods}
    \subsection{Algorithm design}
    The main focus of our research will be analyzing the accuracy and performance of the drowning detection system, with specific attention to determining the best heuristics to detect people who are in distress and finding the optimal topology and weights of the recurrent neural network. Our general goals of our research are the following:
    \begin{itemize}
      \item Determining the key signs of drowning (heuristics) that can be recognized by a computer to tell if a drowning incident is occurring or about to happen.
      
      There are many signs of drowning that human lifeguards use when monitoring pools and beaches. For example, one sign of drowning is not having voluntary control of one's limbs. Our goal is to replicate detecting signs of drowning in software and, more importantly, determine which signs of drowning are the most accurate predictors of drowning incidents. 
      The first step is to determine all possible signs of drowning.
      This will be done by observing drowning incidents and finding patterns in how drowning people behave.
      Then, each possible sign of drowning will be tested for its prediction accuracy on our testing data.
      Our testing data will include video from cameras set up in swimming areas as described in \Cref{datacollection}.
      This process will be repeated for all possible signs of drowning until the most accurate combination of signs of drowning is found.
      
      \item Dynamically learning the significance of signs of drowning
      
      Once the key signs of drowning are identified, we will need to determine which factors are more important than others.
      This requires machine learning, which is using algorithms and pattern recognition to make predictions on data.
      Our machine learning algorithm will use a recurrent neural network (RNN).
      Recurrent neural networks are models of artificial neurons connected by synapses.
      The artificial neurons can be either active or inactive (having values of 1 and 0 respectively), and the synapses have real number ``weights'' that determine how much of an effect each neuron has on the neurons it connects to.
      The receiving neurons apply an activation function to the sum of the weighted input values to determine whether they should be active or inactive.
      By carefully adjusting the weights of the neural network, the network can ``learn'' how to solve particular tasks.
      In addition, the neural network can ``learn'' as it performs its task.
      After we implement the recurrent neural network and put it to work analyzing people swimming, it will be able to learn by itself as it gets more data from the environment.
      Its effectiveness will continue increasing as it receives more data.
      
      Recurrent neural networks have cycles, where neurons can indirectly or directly connect to themselves.
      This allows them to have internal ``memory,'' which can be useful for detecting drowning in rapidly changing conditions.
      For example, if the weather changes and makes the cameras unable to detect certain drowning heuristics, a recurrent neural network would be able to quickly adapt and place more importance on the factors the cameras can detect.
      
      The key issue we will have to solve is determining the layout, or topology, of our recurrent neural network. 
      Determining the topology of the neural network involves determining the amount of input, output, and hidden neurons and the synaptic connections between neurons
      Input neurons receive data directly from the environment, hidden neurons transform that data, and output neurons make the decision whether a person appears to be drowning or not.
      Another component of neural network design is determining how to adjust the weights of the recurrent neural network to have it learn which signs of drowning are the most important.
      There are several learning algorithms that already exist, and we will test those as well as developing our own algorithms and find which learning algorithm is the most effective.
      The most effective algorithm will be determined by how quickly and accurately the network is able to find the relative importance of each sign of drowning.
      For example, if the network quickly learns that screaming is an extremely important sign of drowning, but it turns out that it is actually not important, then that algorithm would not be considered to be effective.
    \end{itemize}
    
    Although there has not been much research in machine learning and pattern recognition, we can still use and build upon the work of other researchers:
    
    \begin{itemize}
      \item Object detection for determining heuristics:
      
      Object detection algorithms and frameworks already exist, but we will be modifying existing work in this field specifically for detecting drowning. The most widely used framework for object detection is the Viola-Jones object detection framework \citep{Souvik-S4-6269796}. This framework was proposed in 2001 by Michael Jones and Paul Viola and is most commonly used for face detection. However, the learning algorithm for the Viola-Jones object detection framework is not specific to face detection and can easily be re-purposed for detecting people in a body of water \citep{Souvik-S8-6094}. In addition, the Viola-Jones learning algorithm uses a variant of AdaBoost to improve learning performance. AdaBoost is a machine-learning algorithm that uses many ``weak'' but fast learning algorithms to generate a ``boosted'' classifier. ``Weak'' learning algorithms have very low threshold values and their results can be close to random. Though AdaBoost uses ``weak'' learning algorithms, the resulting boosted classifier is strong. 
      
      Our object detection algorithm for detecting people in aquatic environments will build upon the work of \cite{Souvik-S5-6255762}, who improved upon the work of Viola and Jones by using local-binary patterns. Local-binary patterns are used in conjunction with support vector machines to detect textures. Some of the issues that come from adapting local-binary patterns for detecting humans in water are sunlight reflection and refraction, which distort the image. To address this issue, color correction and use of technology such as sonar to augment visual data will be used. Our research in heuristics will help determine what exactly drowning looks like to a computer and how to best detect drowning using data from a camera. It is important to develop good heuristics for determining drowning to avoid false positives (or worse, false negatives). 
      
      \item Neural network topology and choosing weights:
      
      Determining neural network topology in particular has not been researched as extensively as pattern recognition heuristics, but nevertheless there has been previous work in this area. There are some rules of thumb researchers currently use, such as $H = O + (0.75I)$ where $H < 2I$ ($H$ is the number of hidden neurons, $O$ is the number of output neurons, and $I$ is the number of input neurons) \citep{Souvik-S6-Shahamiri2014199}. One possible method of identifying the optimal neural network topology is to use genetic algorithms such as NeuroEvolution of Augmenting Topologies (NEAT) \citep{Souvik-S7-NEAT}. NEAT uses simulated genetic mutations and recombinations to evolve neural network topologies using a fitness function, typically defined as the accuracy of the neural network when classifying data. Algorithms like NEAT can also modify synapse weights \citep{Souvik-S7-NEAT}. \Cref{fig:NEAT} shows how neural network topologies and weights can evolve using NEAT.
      
      \begin{figure}[htp!]
        \centering
        \includegraphics[width=0.7\textwidth]{images/iterative_ES_HyperNEAT.jpg}
        \caption{Evolution of a neural network over time using NEAT \citep{Souvik-S8-Risi:2011}}
        \label{fig:NEAT}
      \end{figure}
      
      
    \end{itemize}
    \subsection{Data collection} \label{datacollection}
    In order to train the machine learning algorithm to identify drowning, we need to be able to give it a large amount of data so that it can learn what drowning looks like and what it doesn't look like. 
    Since the intended use will be using a camera feed of a pool or swimming area to detect drowning occurrences, this means we will need to have a large collections of similar videos of pools and swimming areas. 
    Every time someone enters a pool, swims normally, then exits, we can use that video as an instance of normal swimming to train our algorithms as to what normal swimming looks like. 
    However, every time someone swimming requires assistance from a lifeguard or an instance of drowning occurs, that video will be tagged as an instance of when our algorithm should identify that someone is in trouble.
    To collect this data, we will set up cameras in many public pools and swimming areas in the same way they'll be set up once our system is being used. 
    Since pools are public areas, as long as we receive permision from the pools and post notices that there are cameras we are not infringing on privacy.
    Then, we'll wait until enough instances of water emergencies and normal swimming have occurred to train our algorithms.
    The United States Lifesaving Association publishes statistics on the frequency of lifeguard rescues \citep{Sean-S1-DrowningStats}. 
    Using their frequencies of rescues, two rescues can be expected about every month for locations with 100 swimmers a day.  
    However, if we focused our data collection on locations where drownings were more frequent, the ASLA's statistics suggests that we could reasonably expect up to 3 times as many rescues per month.  
    With this in mind, setting up our camera system at around 100 pools or swimming areas and monitoring them for 6 months would give us more than 3,000 rescue training points, which will be more than enough to provide an initial training set to our algorithm.  
    And the beauty of the machine learning algorithm described previously its accuracy will increase as it is used.
    \section{Expected outcomes}
    The data retrieved from local pools and analyzed through artificial intelligence will significantly increase societal understanding of human distress and warning signals. 
    In particular, the research will target visual representations of drowning and identify which cues indicate someone is in need of assistance. 
    By placing cameras above, around, and under the pool, the computer will be able to monitor the entire human body. 
    While it is expected for different humans to react differently in emergency situations, it is nevertheless, very likely, that our research will reveal certain characteristic similarities in displaying distress. 
    It is hypothesized that significant increases in kicking and hand waving can allude to drowning \citep{Evonne-S1-31433556}. 
    
	The data collected from this research will therefore focus primarily on bodily motion and physical responses. 
  However, in order to accurately identify drowning incidents, the data examined must contain both positive controls documenting people actually drowning, and negative controls accounting for people horseplaying in the waters. 
  The goal is to collect a variety of human reactions documenting not only instances of drowning, but also instances of horseplay that include people mimicking the actions of drowning. 
  Samples from either conditions should indicate similarities in both scenarios, but more importantly, should highlight discrepancies in regular and abnormal activities. 
  From the data collected and analysis performed with artificial intelligence, our research will lead to automated lifeguards which will be able to detect signs of drowning more accurately and more immediately than human lifeguards are capable of doing. 
    
	Still, the data collected from our research is expected to reach further than simply detecting and reacting towards distress. 
  Since it is anticipated that in reality, not all instances of distress result in drowning, our live feed should be able to collect scenarios in which a person resurfaces. 
  In such cases, it is preferable for the robot to give the human space to recover by themselves. 
  Therefore, our data will also analyze these situations in a realistic pool setting, differentiating them from actual drowning incidents. 
  Ideally, the programmed robot should be deployed once a distress signal is detected. 
  Upon reaching its destination, however, the robot should perform further analysis to conclude whether or not action is necessary. 
  If the victim has recovered, the robot should return. 
	
    Comprehensively, the data collected across a variety of public pools will be able to define distress signals indicating drowning. 
    The expected outcome is to be able to isolate characteristic signals of drowning that will warrant a preemptive prevention tactic. 
    However, the knowledge gained from this research reaches far beyond the scope of public pools. 
    By analyzing human behavior in an unsimulated environment, the aim is to be able to develop a formula to classify physical cues, differentiating between actions that demand immediate assistance and those that do not.  

    \section{Potential problems}
  The potential problems in this study stem mainly from the reliability of the data collection. 
  Working off of statistics alone, it is very likely that our data collection methods will record a substantial amount of drowning incidents to satisfy the desired number. 
  However, there is the chance that the camera systems that we set up do not capture enough videos of drowning to appropriately train the algorithm that we design. 
  This may be the fault of our machine learning algorithm or the result of a statistically low number of drowning incidents. 
  If this happens, the accuracy of the drowning detection software would be severely lacking, and we would need more time and data to improve it. 
  We would have to extend our data collection period or increase the amount of pools that we are studying. 
  If some of the recorded drowning incidents are obscure or unclear, we may also have to extend our period of collection to ensure we have an appropriate amount of examples of drowning. 
  However, vision obscurity is a challenge that our algorithm will have to tackle in its later stages of development, so these data points are valuable as well. 
  In addition, there is the legality of the data collection methods. 
  People may not want their actions to be recorded and studied. 
  We may have to ask the pools to get permission from its users which may be deter some pools from letting us install camera systems.

  \section{Future applications}
  First of all, the scientific research conducted in this study would contribute to advancements in the fields of human recognition and artificial intelligence. 
  Human actions are incredibly hard to read, and this research will help improve how we translate a camera feed of 2D images to meaningful information about a person's actions to a computer. 
  Machine learning is an equally challenging field that has many intricacies.
  This study can help future studies to better predict the rate of data to accuracy of the algorithm and provide an example of a solid methodology to implement machine learning.

  The immediate practical application of this study is to install a drowning detection system in public pools. 
  Because the pools that we were collecting data in already have the desired camera systems set up, we can simply add a processing unit such as a micro pc to take the camera feed as input and run our drowning identification program and an alarm. 
  This would serve as an aid to human lifeguards to supplement lapses in vision in the confusion of a public pool. 
  As the first stage of implementation, the drowning alarm system could stand as proof that this system does work and that the software does recognize the signs of drowning to a high percentage accuracy rate. 
  In addition, this period of implementation could be used to refine and further increase the accuracy of the program.

  Once the detection system is in place and proven to work, the next logical step is to have the system send out aquatic robots to save the drowning victims. 
  Aquatic robots have already been created and can be adapted to the purpose of mobile flotation devices. 
  Self-navigating aquatic robots using vision have already been researched and created \citep{Calvin-S1-1545231}. 
  The programming of these robots has its own challenges to traverse a crowded pool and avoid people. 
  Also, when the drowning detector identifies a person in need, there is a short time lag between identification and the robot reaching the person. 
  During this time, the person may resurface or the system might realize that this was a false positive. 
  The programming of the robot is incredibly complex and may need to be tackled in a separate study. 
  However, the data and information that we discover from this study can help streamline the process of designing and implementing this new study.

  All of this would eventually, in the far future, culminate in the implementation of a drowning detection and saving system for a public beach. 
  According to the United States Lifesaving association \citep{Sean-S1-DrowningStats}, public beaches are statistically one of the most common places of drowning due to all the confusion. 
  These locations are the worst situations for lifeguards since so much is going on; ideally, this drowning prevention system in the future will be able to make beaches a much safer environment.
              
    \section{Contributions by authors}
    \begin{itemize}
      \item Calvin Ly: Abstract and potential problems and future of this study
      \item Elena Arnold: Originality and significance, objective
      \item Evonne Ng: Expected outcomes
      \item Nolan Daniels: Introduction
      \item Sean Geiger: Co-Authored Methods, Authored ``Data Collection'' section
      \item Souvik Banerjee: Co-Authored Methods, Authored sections ``Heuristics'' and ``Neural network topology and choosing weights,'' formatted document
    \end{itemize}
    \bibliography{references.bib}
\end{document}
